package com.drengi.craftfortress.gamemodes;

public interface GamemodeInterface
{
	public void startRound();
	public void stopRound();
	public void setupMap();
    public boolean isReadyToStart();
    public boolean playersShouldBeFrozen();
    public void beginTimer();
}