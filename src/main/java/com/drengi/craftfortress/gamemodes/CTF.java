package com.drengi.craftfortress.gamemodes;

import java.util.UUID;

import com.drengi.craftfortress.CraftFortress.CraftFortressPlugin;
import com.drengi.craftfortress.CraftFortress.Game;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;

import com.drengi.craftfortress.CraftFortress.ArenaManager;

public class CTF implements GamemodeInterface
{
    private FileConfiguration config = null;
    private Game game = null;
    private String arenaName = null;
    private ArenaManager manager = ArenaManager.getManager();
    private BukkitScheduler sch = CraftFortressPlugin.plugin.getServer().getScheduler();
    
    private boolean playersFrozen = false;
    
    private boolean redFlagPresent = false;
    private boolean blueFlagPresent = false;
    private boolean redSpawnPresent = false;
    private boolean blueSpawnPresent = false;
    //TODO: TEST VARIABLE. DO NOT KEEP IT THIS WAY!
    private boolean arenaReady = true;
    
    int roundCount = 1;
    
    public CTF(Game a)
    {
        game = a;
        arenaName = a.getName();
        config = a.getConfig();
        
        if (config != null)
        {
            //Set up entities.
            if (config.isSet("Entities.redFlag"))
            {
                redFlagPresent = true;
            }
            if (config.isSet("Entities.blueFlag"))
            {
                blueFlagPresent = true;
            }
            if (config.isSet("Entities.redSpawn"))
            {
                redSpawnPresent = true;
            }
            if (config.isSet("Entities.blueSpawn"))
            {
                blueSpawnPresent = true;
            }
        }
        
        if (redFlagPresent && blueFlagPresent && redSpawnPresent && blueSpawnPresent)
        {
            arenaReady = true;
        }
    }
    
    
    @Override
    public boolean isReadyToStart()
    {
        if (arenaReady)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    @Override
    public void startRound()
    {
        CraftFortressPlugin.plugin.getLogger().info("Game " + game.getName() + " round " + roundCount + " beginning.");
        if (game.getPlayerData().size() < 2)
        {
            //If there are less than two players...
        	manager.broadcastArena(game, "WAITING FOR PLAYERS...");
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{ 
            		for (UUID id : game.getPlayerData().keySet())
            		{
            			String team = game.getPlayerData().get(id);
            			Player p = Bukkit.getPlayer(id);
            			manager.respawnPlayer(p, team, game);
            		}
            		
            		manager.broadcastArena(game, "MISSION BEGINS IN 30 SECONDS...");
            		playersFrozen = true;
            	}
            }, 20 * 60);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		manager.broadcastArena(game, "MISSION BEGINS IN 10 SECONDS...");
            	}
            }, 20 * 80);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		manager.broadcastArena(game, "5");
            	}
            }, 20 * 95);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		manager.broadcastArena(game, "4");
            	}
            }, 20 * 96);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		manager.broadcastArena(game, "3");
            	}
            }, 20 * 97);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		manager.broadcastArena(game, "2");
            	}
            }, 20 * 98);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		manager.broadcastArena(game, "1");
            	}
            }, 20 * 99);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		playersFrozen = false;
            		manager.broadcastArena(game, "BEGIN");
            	}
            }, 20 * 100);
        }
        
        //If there are more than two players...
        else 
        {
    		for (UUID id : game.getPlayerData().keySet())
    		{
    			String team = game.getPlayerData().get(id);
    			Player p = Bukkit.getPlayer(id);
    			manager.respawnPlayer(p, team, game);
    		}
        	
        	manager.broadcastArena(game, "MISSION BEGINS IN 30 SECONDS...");
            playersFrozen = true;
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		manager.broadcastArena(game, "MISSION BEGINS IN 10 SECONDS...");
            	}
            }, 20 * 20);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		manager.broadcastArena(game, "5");
            	}
            }, 20 * 35);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		manager.broadcastArena(game, "4");
            	}
            }, 20 * 36);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		manager.broadcastArena(game, "3");
            	}
            }, 20 * 37);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		manager.broadcastArena(game, "2");
            	}
            }, 20 * 38);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		manager.broadcastArena(game, "1");
            	}
            }, 20 * 39);
            
            sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
            { 
            	public void run() 
            	{
            		playersFrozen = false;
            		manager.broadcastArena(game, "BEGIN");
            	}
            }, 20 * 40);
        }
        //TODO: Announcer sound... texture pack?
        
    }
    
    @Override
    public void stopRound() 
    {
        // TODO Auto-generated method stub
    }
    
    @Override
    public void setupMap() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean playersShouldBeFrozen()
    {
		return playersFrozen;
    	
    }
    
    @Override
    public void beginTimer()
    {
        sch.scheduleSyncDelayedTask(CraftFortressPlugin.plugin, new Runnable()
        { 
        	public void run() 
        	{
        		playersFrozen = false;
        		manager.broadcastArena(game, "BEGIN");
        	}
        }, 20 * 40);
    }
    
    
}