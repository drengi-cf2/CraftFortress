package com.drengi.craftfortress.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.drengi.craftfortress.CraftFortress.ArenaManager;

public class PlayerLeave implements Listener
{
	@EventHandler
	public void onLeave(PlayerQuitEvent event) 
	{
	    Player p = event.getPlayer();
		ArenaManager am = ArenaManager.getManager();
		am.removePlayer(p);
	}
}
