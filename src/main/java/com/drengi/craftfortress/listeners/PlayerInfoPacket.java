package com.drengi.craftfortress.listeners;

import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;

import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;
import com.comphenix.protocol.wrappers.EnumWrappers;

import com.drengi.craftfortress.CraftFortress.CraftFortressPlugin;

public class PlayerInfoPacket extends PacketAdapter implements Listener 
{
	boolean saveSkins = false;
	public static HashMap<UUID, String> playerSkins= new HashMap<UUID, String>();
	private YamlConfiguration skins = YamlConfiguration.loadConfiguration(new InputStreamReader(CraftFortressPlugin.plugin.getResource("skins.yml")));

	public PlayerInfoPacket()
	{
		super(CraftFortressPlugin.plugin, ListenerPriority.HIGHEST, PacketType.Play.Server.PLAYER_INFO);
	}
	
	@Override
	public void onPacketSending(PacketEvent event) 
	{
		PacketContainer packet = event.getPacket();
        EnumWrappers.PlayerInfoAction action = packet.getPlayerInfoAction().read(0);
        
        if(action != EnumWrappers.PlayerInfoAction.ADD_PLAYER)
        {
            return;
        }
        
        List<PlayerInfoData> data = packet.getPlayerInfoDataLists().read(0);
        
        for(PlayerInfoData pid : data)
        {
            WrappedGameProfile profile = pid.getProfile();
            
            if (playerSkins.containsKey(profile.getUUID()))
            {
            	profile.getProperties().removeAll("textures");
                WrappedSignedProperty property1 = new WrappedSignedProperty("textures", skins.getString(playerSkins.get(profile.getUUID()) + ".value"), skins.getString( playerSkins.get(profile.getUUID()) + ".signature"));
                Collection<WrappedSignedProperty> properties = new ArrayList<WrappedSignedProperty>();
                properties.add(property1);
                profile.getProperties().putAll("textures", properties);
                properties.remove(profile.getUUID());
            }
            
            if (saveSkins)
            {
                for (WrappedSignedProperty property : profile.getProperties().get("textures"))
                {
                	CraftFortressPlugin.plugin.getLogger().warning("Name: " + property.getName());
                	CraftFortressPlugin.plugin.getLogger().warning("Value:" + property.getValue());
                	CraftFortressPlugin.plugin.getLogger().warning("Signature:" + property.getSignature());
                }
            }
        }
	}
}
