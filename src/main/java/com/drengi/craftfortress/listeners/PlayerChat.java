package com.drengi.craftfortress.listeners;

import com.drengi.craftfortress.CraftFortress.Game;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.drengi.craftfortress.CraftFortress.ArenaManager;

public class PlayerChat implements Listener
{
	@EventHandler
	public void onChat(AsyncPlayerChatEvent event)
	{
		Player eventPlayer = event.getPlayer();
		String msg = event.getMessage();
		ArenaManager am = ArenaManager.getManager();
		Game a = am.searchForPlayer(eventPlayer);
		
		if (a == null)
		{
			am.chatGlobal(eventPlayer, msg);
		}
		else
		{
			am.chatArena(eventPlayer, msg);
		}
		event.setCancelled(true);
	}
}