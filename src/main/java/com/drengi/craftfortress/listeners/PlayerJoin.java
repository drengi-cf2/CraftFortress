package com.drengi.craftfortress.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener 
{	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) 
	{
	    Player p = event.getPlayer();
		
		event.setJoinMessage(ChatColor.WHITE + p.getName() + " connected");
		
	}
}