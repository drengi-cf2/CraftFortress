package com.drengi.craftfortress.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.ArenaManager;
import com.drengi.craftfortress.CraftFortress.CommandInterface;

public class Setup implements CommandInterface {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		Player p = (Player)sender;
		ArenaManager.getManager().setupEntity(p, args[2], args[1], p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ());
		return true;
	}

}
