package com.drengi.craftfortress.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.ArenaManager;
import com.drengi.craftfortress.CraftFortress.CommandInterface;

public class Stop implements CommandInterface
{
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		Player p = (Player) sender;
		if (args.length >= 1)
		{
			String a = args[1];
			
			ArenaManager.getManager().stopArena(a);
			p.sendMessage("Stopping arena now");
		}
		else
		{
			p.sendMessage(ChatColor.RED + "Not enough arguments");
			return false;
		}
		return true;
	}
}
