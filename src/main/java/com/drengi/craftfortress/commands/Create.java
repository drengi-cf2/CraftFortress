package com.drengi.craftfortress.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;

import com.drengi.craftfortress.CraftFortress.Game;
import com.drengi.craftfortress.CraftFortress.CommandInterface;
import com.drengi.craftfortress.CraftFortress.Map;
import com.drengi.craftfortress.CraftFortress.CraftFortressPlugin;

public class Create implements CommandInterface
{
	public WorldEditPlugin worldeditplugin = null;

	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length > 3)
		{
			try
			{
				Player p = (Player)sender;
				worldeditplugin = (WorldEditPlugin) CraftFortressPlugin.plugin.getServer().getPluginManager().getPlugin("WorldEdit");
				Selection sel = worldeditplugin.getSelection(p);
				
				if (sel != null)
				{
					World world = sel.getWorld();
                    Location minpoint = sel.getMinimumPoint();
                    Location maxpoint = sel.getMaximumPoint();
                    
                    String worldName = world.getName().toString();
                    String type = args[1];
                    String name = args[2];
					String gm = args[3];
					if (type.equalsIgnoreCase("arena"))
					{
						Game a = new Game(name);
						a.createArena(gm);
						p.sendMessage(ChatColor.GREEN + "Done. If you've created a map already, you can attach it with /cf2 attach <arena> <map> ");
					}
					else if (type.equalsIgnoreCase("map"))
					{
						Map m = new Map(name);
						m.createMap(worldName, minpoint.getX(), minpoint.getY(), maxpoint.getX(), maxpoint.getY(), gm);
						p.sendMessage(ChatColor.GREEN + "Done. Please set the spawns with /cf2 setspawn <type> <name> ");
					}
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return true;
	}
}