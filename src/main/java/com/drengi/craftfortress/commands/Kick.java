package com.drengi.craftfortress.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.ArenaManager;
import com.drengi.craftfortress.CraftFortress.CommandInterface;

public class Kick implements CommandInterface {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		Player p = (Player) sender;
		
		if (p.hasPermission("com.drengi.craftfortress.CraftFortress.admin.kick"))
		{
			if (args.length > 1)
			{
				StringBuilder sb = new StringBuilder();
				
				for (int i = 2; i < args.length; i++)
				{
					sb.append(args[i]).append(" ");
				}
				 
				String m = sb.toString().trim();
				
				ArenaManager.getManager().kickPlayer(Bukkit.getPlayer(args[1]), m);
			}
		}
		else
		{
			p.sendMessage(ChatColor.RED + "You don't have permission to perform this command.");
		}
		return true;
	}

}
