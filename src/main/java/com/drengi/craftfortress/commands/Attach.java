package com.drengi.craftfortress.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.ArenaManager;
import com.drengi.craftfortress.CraftFortress.CommandInterface;

public class Attach implements CommandInterface
{
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		Player p = (Player) sender;
		if (args.length > 2)
		{
			String map = args[1];
			String arena = args[2];
			
			if (ArenaManager.getManager().checkIfArenaLoaded(arena))
			{
				if (ArenaManager.getManager().checkIfMapLoaded(map))
				{
					ArenaManager.getManager().attachMap(map, arena);
					p.sendMessage(ChatColor.GREEN + "Map attached to arena successfully");
				}
				else
				{
					p.sendMessage("Are you sure that map is setup and loaded?");
				}
			}
			else
			{
				p.sendMessage("Are you sure that arena is setup and loaded?");
			}
		}
		else
		{
			p.sendMessage(ChatColor.RED + "/cf2 attach <map> <arena>");
			return false;
		}
		return true;
	}
}
