package com.drengi.craftfortress.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.ArenaManager;
import com.drengi.craftfortress.CraftFortress.CommandInterface;

public class Start implements CommandInterface
{
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		Player p = (Player) sender;
		if (args.length >= 1)
		{
			String a = args[1];
			
			if (ArenaManager.getManager().checkIfArenaExists(a))
			{
				ArenaManager.getManager().startArena(a);
				p.sendMessage("Starting arena now");
			}
			else
			{
				p.sendMessage(ChatColor.RED + "Are you sure that arena exists?");
			}
		}
		else
		{
			p.sendMessage(ChatColor.RED + "Not enough arguments");
			return false;
		}
		return true;
	}
}
