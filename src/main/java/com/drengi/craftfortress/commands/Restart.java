package com.drengi.craftfortress.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.drengi.craftfortress.CraftFortress.ArenaManager;
import com.drengi.craftfortress.CraftFortress.CommandInterface;
import net.md_5.bungee.api.ChatColor;

public class Restart implements CommandInterface {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		ArenaManager.getManager().restartArena(args[1]);
		sender.sendMessage(ChatColor.GREEN + "Game restarted.");
		return true;
	}

}
