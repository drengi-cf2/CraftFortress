package com.drengi.craftfortress.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.ArenaManager;
import com.drengi.craftfortress.CraftFortress.CommandInterface;
import net.md_5.bungee.api.ChatColor;

public class Setspawn implements CommandInterface
{
	boolean success = false;

	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		if (sender instanceof Player)
		{
			Player p = (Player)sender;
			
			if (args.length == 3)
			{
				Location loc = p.getLocation();
				String type = args[1];
				String name = args[2];
				
				if (ArenaManager.getManager().checkIfMapExists(name))
				{
					ArenaManager.getManager().setSpawn(name, type, loc.getX(), loc.getY(), loc.getZ());
					p.sendMessage(ChatColor.GREEN + "Good. The spawn has been setup. If you've added both spawns, the map will load automagically");
				}
				else
				{
					p.sendMessage(ChatColor.RED + "Does that map exist?");
				}
			}
			else
			{
				p.sendMessage(ChatColor.RED + "error with arguments");
			}
		}
		else
		{
			sender.sendMessage("You must be a player to do this");
		}
		return true;
	}
}