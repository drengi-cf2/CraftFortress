package com.drengi.craftfortress.commands;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.CommandInterface;

public class Help implements CommandInterface
{
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		Player p = (Player) sender;
		p.sendMessage(ChatColor.GOLD + "------ com.drengi.craftfortress.CraftFortress 0.1.2 Help ------");
		//If CandSmine
		if (p.getUniqueId() == UUID.fromString("2ab174fc-5a06-4b0e-b4d3-453a33d92858"));
		{
			p.sendMessage(ChatColor.RED + "Wait, hold on, what are you doing on this server?");
			p.sendMessage(ChatColor.RED + "I think I've banned you enough times for you to not come back");
			p.sendMessage(ChatColor.RED + "But hey, increase my player count, that's cool.");
			p.sendMessage("");
			p.sendMessage(ChatColor.RED + "And don't do anything stupid. Thanks");
			p.sendMessage("");
		}
		
		if (p.hasPermission("com.drengi.craftfortress.CraftFortress.com.drengi.craftfortress.chat.global"))
		{
			p.sendMessage("/global: Sends a com.drengi.craftfortress.chat message to the entire server.");
		}
		
		if (p.hasPermission("com.drengi.craftfortress.CraftFortress.com.drengi.craftfortress.chat.admin"))
		{
			p.sendMessage("/ac: Talk in admin com.drengi.craftfortress.chat");
		}
		
		return true;
	}
}