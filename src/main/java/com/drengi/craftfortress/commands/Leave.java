package com.drengi.craftfortress.commands;

import com.drengi.craftfortress.CraftFortress.CraftFortressPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.ArenaManager;
import com.drengi.craftfortress.CraftFortress.CommandInterface;

public class Leave implements CommandInterface
{
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if (!(sender instanceof Player))
        {
			CraftFortressPlugin.plugin.getLogger().warning("You cannot leave, you're the console!");
        }
		else
		{
			Player p = (Player) sender;
			ArenaManager am = ArenaManager.getManager();
			am.removePlayer(p);
		}
		return true;
	}
}
