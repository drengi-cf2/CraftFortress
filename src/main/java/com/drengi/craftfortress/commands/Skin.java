package com.drengi.craftfortress.commands;

import com.drengi.craftfortress.CraftFortress.ArenaManager;
import com.drengi.craftfortress.CraftFortress.CommandInterface;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Skin implements CommandInterface
{
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        ArenaManager.getManager().setSkin((Player) sender, "redheavy");
        return true;
    }
}
