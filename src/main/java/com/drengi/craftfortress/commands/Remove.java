package com.drengi.craftfortress.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.ArenaManager;
import com.drengi.craftfortress.CraftFortress.CommandInterface;

public class Remove implements CommandInterface
{
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{	
		Player p = (Player) sender;
		ArenaManager.getManager().deleteArena(args[1]);
		p.sendMessage(ChatColor.GREEN + "Game deleted");
		return true;
	}
}
