package com.drengi.craftfortress.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.ArenaManager;
import com.drengi.craftfortress.CraftFortress.CommandInterface;

public class Join implements CommandInterface
{	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		Player p = (Player) sender;
		if (args.length > 2)
		{
			String a = args[1];
			String t = args[2];
			
			ArenaManager.getManager().addPlayer(p, a, t);
		}
		else
		{
			p.sendMessage(ChatColor.RED + "You must include both the map and the team you want to join, for now.");
			return false;
		}
		return true;
	}
}