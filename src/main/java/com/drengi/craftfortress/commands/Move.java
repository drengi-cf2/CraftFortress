package com.drengi.craftfortress.commands;

import com.drengi.craftfortress.CraftFortress.CraftFortressPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.ArenaManager;
import com.drengi.craftfortress.CraftFortress.CommandInterface;

public class Move implements CommandInterface
{	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		Player p = (Player) sender;
		if (args.length == 2)
		{
			String team = args[1];
			if (ArenaManager.getManager().searchForPlayer(p) == null)
			{
				p.sendMessage(ChatColor.RED + "Um, what's of the point of switching if you aren't playing?");
			}
			else if (team != "RED" && team != "BLU" && team != "Spectator")
			{
				p.sendMessage(ChatColor.RED + "Valid team options are RED, BLU, and Spectator");
			}
			else
			{
				ArenaManager.getManager().movePlayer(p, team);
			}
		}
		else if (args.length > 2)
		{
			String player = args[1];
			String team = args[2];
			
			Player Player = CraftFortressPlugin.plugin.getServer().getPlayer(player);
			if (Player == null)
			{
				p.sendMessage(ChatColor.RED + "That player does not exist!");
			}
			else
			{
				if (ArenaManager.getManager().searchForPlayer(Player) == null)
				{
					p.sendMessage(ChatColor.RED + "That player is not in an arena.");
				}
				else
				{
					ArenaManager.getManager().movePlayer(Player, team);
				}
			}
		}
		return true;
	}
}