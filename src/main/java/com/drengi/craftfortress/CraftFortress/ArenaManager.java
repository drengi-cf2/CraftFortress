package com.drengi.craftfortress.CraftFortress;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import com.drengi.craftfortress.listeners.PlayerInfoPacket;
import com.drengi.craftfortress.wrappers.WrapperPlayServerPlayerInfo;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.google.common.collect.Lists;

public class ArenaManager
{
    public final HashMap<String, Game> loadedArenas = new HashMap<String, Game>();
    public final HashMap<String, Game> enabledArenas = new HashMap<String, Game>();
    
    public final HashMap<String, Map> loadedMaps = new HashMap<String, Map>();
    public final HashMap<String, Map> enabledMaps = new HashMap<String, Map>();
    public final HashMap<String, Map> availableMaps = new HashMap<String, Map>();
    
    private final FileConfiguration config = CraftFortressPlugin.plugin.getConfig();
    private Logger log = CraftFortressPlugin.plugin.getLogger();
    ConsoleCommandSender console = CraftFortressPlugin.plugin.getServer().getConsoleSender();
    
    private static ArenaManager am;
    
    private ArenaManager() {}
    
    public static ArenaManager getManager()
    {
        if (am == null)
            am = new ArenaManager();
        
        return am;
    }
    
    //Just so I stop confusing myself, this still calls loadArena!
    public void loadArenas()
    {
        log.info("Game loading has begun");
        
        try
        {
            for (String arenaName : config.getConfigurationSection("Arenas").getKeys(false))
            {
            	loadArena(arenaName, true);
            }
        }
        catch (Exception e)
        {
            log.warning("Either there are no maps, or your config file is malformed.");
            e.printStackTrace();
        }
    }
    
    private void loadArena(String arenaName, boolean msg)
    {
        if(this.checkIfArenaExists(arenaName) && !loadedArenas.containsKey(arenaName))
        {
            Game game = new Game(arenaName);
            
            if (game.loadArena())
            {
                log.info("Game " + game.getName() + " loaded!");
                loadedArenas.put(game.getName(), game);
                if (game.isEnabled())
                {
                	this.startArena(arenaName);
                }
                else
                {
                	if (msg)
                	{
                		log.warning("Game " + game.getName() + " is not enabled. Start it with /cf2 start " + arenaName);
                	}
                }
                
            }
            else
            {
                log.warning("Did you forget to setup " + arenaName + "?");
            }
        }
    }
    
    public void loadMaps()
    {
        log.info("Map loading has begun");
        
        try
        {
            for (String mapName : config.getConfigurationSection("Maps").getKeys(false))
            {
            	loadMap(mapName);
            }
        }
        catch (Exception e)
        {
            log.warning("Either there are no maps, or your config file is malformed.");
            e.printStackTrace();
        }
    }
    
    private void loadMap(String mapName)
    {
        if(!loadedMaps.containsKey(mapName))
        {
            Map map = new Map(mapName);
            
            if (map.loadMap())
            {
                log.info("Map " + map.getName() + " loaded!");
                loadedMaps.put(map.getName(), map);
                if (map.isEnabled())
                {
                	enabledMaps.put(map.getName(), map);
                	availableMaps.put(map.getName(), map);
                }
                else
                {
                	log.warning("Map " + map.getName() + " is not setup completely. You should finish setting it up.");
                }
                
            }
            else
            {
                log.warning("There was an issue loading map " + mapName);
            }
        }
    }
    
    private Game getArena(String name)
    {
        if (this.loadedArenas.containsKey(name))
        {
            Game a = this.loadedArenas.get(name);
            return a;
        }
        else
        {
            return null;
        }
    }
    
    protected Map getMap(String name)
    {
    	if (this.loadedMaps.containsKey(name))
    	{
    		Map m = this.loadedMaps.get(name);
    		return m;
    	}
    	else
    	{
    		return null;
    	}
    }
    
    public void attachMap(String map, String arena)
    {
    	Game a = this.getArena(arena);
    	a.attachMap(this.getMap(map));
    }
    
    public void reloadArenas()
    {
        this.loadedArenas.clear();
        this.loadArenas();
    }
    
    public Game searchForPlayer(Player p)
    {
        Game game = null;
        for (String a : loadedArenas.keySet())
        {
            Game gameToReturn = loadedArenas.get(a);
            HashMap<UUID, String> players = gameToReturn.getPlayerData();
            for (UUID u : players.keySet())
            {
                if (u.equals(p.getUniqueId()))
                {
                    game = gameToReturn;
                }
            }
        }
        return game;
    }
    
    public void addPlayer(Player p, String n, String t)
    {
        Game a = null;
        if (!checkIfStarted(n))
        {
            p.sendMessage(ChatColor.RED + "This arena does not exist");
            return;
        }
        else
        {
        	a = this.getArena(n);
        }
        
        if (t.equalsIgnoreCase("BLU") || t.equalsIgnoreCase("RED") || t.equalsIgnoreCase("SPEC"))
        {
            a.getPlayerData().put(p.getUniqueId(), t);
            Scoreboard board = a.getBoard();
            
            if (t.equalsIgnoreCase("BLU"))
            {
                a.getBLU().addPlayer(p);
                p.setScoreboard(board);
                this.broadcastArena(a, p.getDisplayName() + " joined team " + ChatColor.BLUE + "BLU");
                p.teleport(a.getSpawn("BLU"));
                return;
            }
            if (t.equalsIgnoreCase("RED"))
            {
                a.getRED().addPlayer(p);
                p.setScoreboard(board);
                this.broadcastArena(a, p.getDisplayName() + " joined team " + ChatColor.RED + "RED");
                p.teleport(a.getSpawn("RED"));
                return;
            }
            if (t.equalsIgnoreCase("SPEC"))
            {
                a.getSpec().addPlayer(p);
                p.setScoreboard(board);
                this.broadcastArena(a, p.getDisplayName() + " joined team " + ChatColor.WHITE + "Spectator");
                p.teleport(a.getSpawn("SPEC"));
                return;
            }
        }
    }
    
    public void removePlayer(Player p)
    {
        Game a = this.searchForPlayer(p);
        if (a == null)
        {
            p.sendMessage("Invalid Game");
            return;
        }
        
        String team = a.getPlayerData().get(p.getUniqueId());
        a.getPlayerData().remove(p.getUniqueId());
        
        if (team.equalsIgnoreCase("BLU"))
        {
            a.getBLU().removePlayer(p);
            a.getPlayerData().remove(p.getUniqueId());
            this.broadcastArena(a, p.getDisplayName() + " left team " + ChatColor.BLUE + "BLU");
            p.setScoreboard(CraftFortressPlugin.manager.getNewScoreboard());
            p.teleport(Bukkit.getWorld("world").getSpawnLocation());
            return;
        }
        if (team.equalsIgnoreCase("RED"))
        {
            a.getRED().removePlayer(p);
            a.getPlayerData().remove(p.getUniqueId());
            this.broadcastArena(a, p.getDisplayName() + " left team " + ChatColor.RED + "RED");
            p.setScoreboard(CraftFortressPlugin.manager.getNewScoreboard());
            p.teleport(Bukkit.getWorld("world").getSpawnLocation());
            return;
        }
        if (team.equalsIgnoreCase("SPEC"))
        {
            a.getSpec().removePlayer(p);
            a.getPlayerData().remove(p.getUniqueId());
            this.broadcastArena(a, p.getDisplayName() + " left team " + ChatColor.WHITE + "Spectator");
            p.setScoreboard(CraftFortressPlugin.manager.getNewScoreboard());
            p.teleport(Bukkit.getWorld("world").getSpawnLocation());
            return;
        }
        else
        {
            p.sendMessage("Invalid Team.");
        }
        
    }
    
    public void movePlayer(Player p, String t)
    {
        Game a = searchForPlayer(p);
        if (a == null)
        {
            p.sendMessage("Invalid Game");
            return;
        }
        
        String team = a.getPlayerData().get(p.getUniqueId());
        a.getPlayerData().remove(p.getUniqueId());
        
        if (team.equalsIgnoreCase("RED"))
        {
            a.getRED().removePlayer(p);
            if (t.equalsIgnoreCase("BLU"))
            {
            	a.getBLU().addPlayer(p);
            	this.broadcastArena(a, p.getDisplayName() + " has switched to " + ChatColor.BLUE + "BLU");
            	a.getPlayerData().put(p.getUniqueId(), "BLU");
            	p.teleport(a.getSpawn("BLU"));
                
            }
            else if (t.equalsIgnoreCase("Spectator"))
            {
            	a.getSpec().addPlayer(p);
            	this.broadcastArena(a, p.getDisplayName() + " has switched to " + ChatColor.WHITE + "Spectator");
                a.getPlayerData().put(p.getUniqueId(), "SPEC");
                p.teleport(a.getSpawn("SPEC"));
            }
            else
            {
                p.sendMessage("You cannot join this team.");
            }
        }
        
        else if (team.equalsIgnoreCase("BLU"))
        {
            a.getRED().removePlayer(p);
            if (t.equalsIgnoreCase("RED"))
            {
            	a.getRED().addPlayer(p);
            	this.broadcastArena(a, p.getDisplayName() + " has switched to " + ChatColor.RED + "RED");
                a.getPlayerData().put(p.getUniqueId(), "RED");
                p.teleport(a.getSpawn("RED"));
            }
            else if (t.equalsIgnoreCase("Spectator"))
            {
            	a.getSpec().addPlayer(p);
            	this.broadcastArena(a, p.getDisplayName() + " has switched to " + ChatColor.WHITE + "Spectator");
                a.getPlayerData().put(p.getUniqueId(), "SPEC");
                p.teleport(a.getSpawn("SPEC"));
            }
            else
            {
                p.sendMessage("You cannot join this team.");
            }
        }
        else if (team.equalsIgnoreCase("SPEC"))
        {
            a.getRED().removePlayer(p);
            if (t.equalsIgnoreCase("RED"))
            {
            	a.getRED().addPlayer(p);
            	this.broadcastArena(a, p.getDisplayName() + " has switched to " + ChatColor.RED + "RED");
                a.getPlayerData().put(p.getUniqueId(), "RED");
                p.teleport(a.getSpawn("RED"));
            }
            else if (t.equalsIgnoreCase("BLU"))
            {
            	a.getBLU().addPlayer(p);
            	this.broadcastArena(a, p.getDisplayName() + " has switched to " + ChatColor.BLUE + "BLU");
                a.getPlayerData().put(p.getUniqueId(), "BLU");
                p.teleport(a.getSpawn("BLU"));
            }
            else
            {
                p.sendMessage("You cannot join this team.");
            }
        }
    }
    
    public void respawnPlayer(Player p, String t, Game a)
    {
    	p.teleport(a.getSpawn(t));
    }
    
    public void kickPlayer(Player p, String msg)
    {
    	Game a = this.searchForPlayer(p);
    	this.removePlayer(p);
    	this.broadcastArena(a, p.getDisplayName() + " was kicked from the arena. Reason: " + msg);
    	p.sendMessage(ChatColor.RED + "You were kicked from the arena. Reason: " + msg);
    }
    
    public void chatArena(Player p, String m)
    {
        String n = p.getDisplayName();
        Game a = searchForPlayer(p);
        
        for (UUID u : a.getPlayerData().keySet())
        {
            Player player = Bukkit.getPlayer(u);
            String team = a.getPlayerData().get(u);
            ChatColor color = null;
            if (team.equalsIgnoreCase("RED")) { color = ChatColor.RED; }
            if (team.equalsIgnoreCase("BLU")) { color = ChatColor.BLUE; }
            if (team.equalsIgnoreCase("SPEC")) { color = ChatColor.WHITE; }
            player.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + a.getName().toUpperCase() + ChatColor.DARK_GRAY + "] " + color + n + ": " + ChatColor.RESET + m);
            console.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + a.getName().toUpperCase() + ChatColor.DARK_GRAY + "] " + color + n + ": " + ChatColor.RESET + m);
        }
    }
    
    public void chatGlobal(Player p, String m)
    {
        String n = p.getDisplayName();
        Bukkit.broadcastMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + "GLOBAL" + ChatColor.DARK_GRAY + "] " + ChatColor.WHITE + n + ": " + ChatColor.RESET + m);
    }
    
    public void chatAdmin(String p, String m)
    {
        for (Player player : Bukkit.getServer().getOnlinePlayers())
        {
            if (player.hasPermission("craftfortress.admin.com.drengi.craftfortress.chat"))
            {
                player.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + "ADMIN CHAT" + ChatColor.DARK_GRAY + "] "  + ChatColor.WHITE + p + ": " + ChatColor.RESET + m);
                console.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + "ADMIN CHAT" + ChatColor.DARK_GRAY + "] " + ChatColor.WHITE + p + ": " + ChatColor.RESET + m);
            }
        }
    }
    
    public void chatTeam(Player p, String m)
    {
        Game a = this.searchForPlayer(p);
        if (a != null)
        {
            if (a.getBLU().hasPlayer(p))
            {
                //Player is on BLU
                for (OfflinePlayer player : a.getBLU().getPlayers())
                {
                    player.getPlayer().sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.BLUE + "TEAM" + ChatColor.DARK_GRAY + "] "  + ChatColor.BLUE + p.getName() + ": " + ChatColor.RESET + m);
                    console.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + a.getName().toUpperCase() + ChatColor.DARK_GRAY + "] " + ChatColor.DARK_GRAY + "[" + ChatColor.BLUE + "BLU" + ChatColor.DARK_GRAY + "] "  + ChatColor.WHITE + p.getName() + ": " + ChatColor.RESET + m);
                    
                }
            }
            else if (a.getRED().hasPlayer(p))
            {
                for (OfflinePlayer player : a.getRED().getPlayers())
                {
                    player.getPlayer().sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "TEAM" + ChatColor.DARK_GRAY + "] "  + ChatColor.RED + p.getName() + ": " + ChatColor.RESET + m);
                    console.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + a.getName().toUpperCase() + ChatColor.DARK_GRAY + "] " + ChatColor.DARK_GRAY + "[" + ChatColor.RED + "RED" + ChatColor.DARK_GRAY + "] "  + ChatColor.WHITE + p.getName() + ": " + ChatColor.RESET + m);
                }
            }
            else if (a.getSpec().hasPlayer(p))
            {
                this.chatArena(p, m);
            }
        }
        else
        {
            p.sendMessage(ChatColor.RED + "You need to  be in an arena to do this.");
        }
    }
    
    public void broadcastArena(Game a, String m)
    {
        for (UUID u : a.getPlayerData().keySet())
        {
            Player player = Bukkit.getPlayer(u);
            player.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + a.getName().toUpperCase() + ChatColor.DARK_GRAY + "] " + ChatColor.RESET + m);
        }
        console.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + a.getName().toUpperCase() + ChatColor.DARK_GRAY + "] " + ChatColor.RESET + m);
    }
    
    public String checkBalance(Game a, boolean autoBalance)
    {
        int redBal = a.getRED().getSize();
        int bluBal = a.getBLU().getSize();
        String teamUnbalanced = null;
        
        if (redBal > bluBal)
        {
            if (autoBalance)
            {
                if ((redBal - bluBal) > 1)
                {
                    teamUnbalanced = "RED";
                }
            }
            else
            {
                teamUnbalanced = "RED";
            }
        }
        else if (redBal < bluBal)
        {
            if (autoBalance)
            {
                if ((bluBal - redBal > 1))
                {
                    teamUnbalanced = "BLU";
                }
            }
            else
            {
                teamUnbalanced = "BLU";
            }
        }
        else
        {
            teamUnbalanced = null;
        }
        
        return teamUnbalanced;
    }
    
    public HashMap<Player, String> shuffleTeams(Game a)
    {
        //Add currently existing players to one array list
        ArrayList<Player> allPlayers = new ArrayList<Player>();
        
        for (OfflinePlayer P : a.getBLU().getPlayers())
        {
            Player player = P.getPlayer();
        	allPlayers.add(player);
        }
        
        for (OfflinePlayer P : a.getRED().getPlayers())
        {
        	Player player = P.getPlayer();
        	allPlayers.add(player);
        }
        
        //Randomize teams
        Random gen = new Random();
        int bluCount = 0;
        int redCount = 0;
        HashMap<Player, String> shuffledPlayers = new HashMap<Player, String>();
        
        for (Player p : allPlayers)
        {
            if (gen.nextInt(1) == 0 && bluCount < 12)
            {
                shuffledPlayers.put(p, "BLU");
            }
            else if (gen.nextInt(1) == 1 && redCount < 12)
            {
                shuffledPlayers.put(p, "RED");
            }
            else
            {
                //An error occurred during shuffling or there are too many players which should never happen
                shuffledPlayers.put(p, "SPEC");
            }
        }
        
        //Final sanity checks (cause this could really screw up gameplay if something goes wrong.
        if (shuffledPlayers.size() != allPlayers.size())
        {
            log.warning("Sorter: Original player list does not match shuffled player list!");
            return null;
        }
        
        return shuffledPlayers;
    }
    
    public boolean checkIfArenaExists(String n)
    {
        boolean status = false;
        for (String arenaName : config.getConfigurationSection("Arenas").getKeys(false))
        {
            if(arenaName.equalsIgnoreCase(n))
            {
                status = true;
                break;
            }
        }
        return status;
    }
    
    public boolean checkIfMapExists(String n)
    {
        boolean status = false;
        for (String mapName : config.getConfigurationSection("Maps").getKeys(false))
        {
            log.warning(mapName);
        	if(mapName.equalsIgnoreCase(n))
            {
                status = true;
                break;
            }
        }
        return status;
    }
    
    public boolean checkIfMapAvailable(String n)
    {
    	return availableMaps.containsKey(n);
    }
    
    public boolean checkIfStarted(String n)
    {
    	if (enabledArenas.containsKey(n))
    	{
    		return true;
    	}
    	return false;
    }
    
    public boolean checkIfArenaLoaded(String n)
    {
    	if (loadedArenas.containsKey(n))
    	{
    		return true;
    	}
    	return false;
    }
    
    public boolean checkIfMapLoaded(String n)
    {
    	if (loadedMaps.containsKey(n))
    	{
    		return true;
    	}
    	return false;
    }
    
    public void restartArena(String n)
    {
        log.info("Restarting arena " + n);
        if (this.stopArena(n))
        {
            if (this.startArena(n))
            {
                log.info("The restart arena " + n + " succeeded");
            }
        }
    }
    
    public boolean stopArena(String n)
    {
        boolean success = false;
        if (this.checkIfStarted(n))
        {
            Game a = this.getArena(n);
            log.info("Shutting down arena " + n);
            this.broadcastArena(a, "This arena is being shut down.");
            for (UUID u : a.getPlayerData().keySet())
            {
                Player p = Bukkit.getPlayer(u);
                this.removePlayer(p);
            }
            
            enabledArenas.remove(n);
            success = true;
            
            if (success)
            {
                log.info("Shutting down arena " + n + " done!");
            }
            else
            {
                log.info("Shutting down arena " + n + " failed!");
            }
        }
        else
        {
            success = false;
        }
        return success;
    }
    
    public boolean startArena(String n)
    {
        boolean success = false;
        if (checkIfArenaLoaded(n) && !checkIfStarted(n))
        {
        	if (loadedArenas.get(n).getGamemode().isReadyToStart() && loadedArenas.get(n).hasMaps())
        	{
        		loadedArenas.get(n).enableArena();
        		loadedArenas.get(n).start();
        		enabledArenas.put(n, loadedArenas.get(n));
        		log.info("Game " + n + " started successfully");
        	}
        }
        else
        {
        	loadArena(n, false);
        	if (checkIfArenaLoaded(n) && !checkIfStarted(n))
        	{
            	if (loadedArenas.get(n).getGamemode().isReadyToStart() && loadedArenas.get(n).hasMaps())
            	{
            		loadedArenas.get(n).enableArena();
            		loadedArenas.get(n).getGamemode().startRound();
            		enabledArenas.put(n, loadedArenas.get(n));
            		log.info("Game " + n + " started successfully");
            	}
        	}
        }
        return success;
    }
    
    public boolean deleteArena(String n)
    {
        log.info("Deleting arena " + n);
        this.stopArena(n);
        loadedArenas.remove(n);
        File file = new File(CraftFortressPlugin.plugin.getDataFolder() + File.separator + "arenas" + File.separator + n + ".yml");
        
        if (file.delete())
        {
            config.set("Arenas." + n, null);
            CraftFortressPlugin.plugin.saveConfig();
            log.info("Deleted arena " + n);
            return true;
        }
        log.warning("Could not delete arena " + n);
        return false;
    }
    
    public void setSpawn(String n, String type, double x, double y, double z)
    {
    	if (checkIfMapLoaded(n))
    	{
    		Map m = getMap(n);
    		m.setSpawn(type, x, y, z);
    		if (m.checkIfMeetsRequirements())
    		{
    			m.enable();
    			this.enabledMaps.put(n, m);
            	this.availableMaps.put(n, m);
    		}
    		log.info("Setting spawn of arena " + n + " to " + x + y + z);
    	}
    	
    	else if (checkIfMapExists(n))
    	{
    		Map m = new Map(n);
            m.setSpawn(type, x, y, z);
            if (m.checkIfMeetsRequirements()) 
            {
            	m.enable();
            	this.loadedMaps.put(n, m);
            	this.enabledMaps.put(n, m);
            	this.availableMaps.put(n, m);
            }
            log.info("Setting spawn of arena " + n + " to " + x + y + z);
    	}
    }
    
    public void setupEntity(Player p, String ent, String map, double x, double y, double z)
    {
        if (this.checkIfMapLoaded(map))
        {
            Map m = this.getMap(map);
            m.createEntity(p, ent, x, y, z);
        }
        else
        {
        	if (checkIfMapExists(map))
        	{
            	Map m = new Map(map);
            	m.createEntity(p, ent, x, y, z);
        	}
        }
    }
    
    public void setSkin(Player p, String className)
    {
		PlayerInfoPacket.playerSkins.put(p.getUniqueId(), className);
    	//Update TabList
    	WrapperPlayServerPlayerInfo packet = new WrapperPlayServerPlayerInfo();
		packet.setAction(EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
		WrappedGameProfile profile = new WrappedGameProfile(p.getUniqueId(), null);
		packet.setData(Lists.newArrayList(new PlayerInfoData(profile, 0, EnumWrappers.NativeGameMode.SURVIVAL, null)));

		for (Player plr : CraftFortressPlugin.plugin.getServer().getOnlinePlayers())
		{
			if (plr.canSee(p)) 
			{
				packet.sendPacket(plr);
			}
		}
		
		WrapperPlayServerPlayerInfo basePacket = new WrapperPlayServerPlayerInfo();
		basePacket.setAction(EnumWrappers.PlayerInfoAction.ADD_PLAYER);
		WrappedGameProfile profile1 = new WrappedGameProfile(p.getUniqueId(), p.getName());
		final EnumWrappers.NativeGameMode nativeGameMode = EnumWrappers.NativeGameMode.fromBukkit(p.getGameMode());

		int ping = getPing(p);
		// send to all players
		for (Player plr : CraftFortressPlugin.plugin.getServer().getOnlinePlayers())
		{
			// ... except the ones who are hidden from the player
			if (plr.canSee(p)) 
			{
				//create and send the packet
				WrapperPlayServerPlayerInfo packet1 = new WrapperPlayServerPlayerInfo(basePacket.getHandle());
				packet1.setData(Collections.singletonList(new PlayerInfoData(profile1, ping, nativeGameMode, WrappedChatComponent.fromText(p.getName()))));
				packet1.sendPacket(plr); //invokes our own packet handler (this class) as this does not set any skin-related data
    
			}
		}
		
		//Update skin
		Collection<? extends Player> seeablePlayers = new HashSet<>(CraftFortressPlugin.plugin.getServer().getOnlinePlayers());
		seeablePlayers.removeAll(p.spigot().getHiddenPlayers());
		for (Player plr : seeablePlayers) 
		{
			plr.hidePlayer(p);
		}
			// show the players again with a little delay to prevent issues
		for (Player plr : seeablePlayers) 
		{
				plr.showPlayer(p);
		}
	}
    
	private int getPing(Player target) 
	{
		int ping;
		Class<?> craftPlayer = target.getClass();
		try 
		{
			Method getHandle = craftPlayer.getMethod("getHandle", (Class[]) null);
			Object entityPlayer = getHandle.invoke(target);
			Field pingField = entityPlayer.getClass().getField("ping");
			ping = (int) pingField.get(entityPlayer);
		} 
		catch (ReflectiveOperationException e) 
		{
			CraftFortressPlugin.plugin.getLogger().warning("Could not get ping of player " + target.getName() + ". Error:");
			e.printStackTrace();
			ping = 0;
		}
		return ping;
	}
}