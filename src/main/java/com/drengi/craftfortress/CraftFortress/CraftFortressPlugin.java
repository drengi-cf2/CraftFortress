package com.drengi.craftfortress.CraftFortress;

import com.drengi.craftfortress.commands.*;
import com.drengi.craftfortress.listeners.PlayerChat;
import com.drengi.craftfortress.listeners.PlayerInfoPacket;
import com.drengi.craftfortress.listeners.PlayerJoin;
import com.drengi.craftfortress.listeners.PlayerLeave;

import java.lang.reflect.Method;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.ScoreboardManager;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;

import com.drengi.craftfortress.chat.Global;
import com.drengi.craftfortress.chat.Team;
import com.drengi.craftfortress.chat.Admin;

public class CraftFortressPlugin extends JavaPlugin
{
	public final FileConfiguration config = getConfig();
	public WorldEditPlugin worldeditplugin = null;
	public static ScoreboardManager manager = null;
	public ProtocolManager protocol;
	public static CraftFortressPlugin plugin;
	
	public volatile static Collection<WrappedSignedProperty> properties;

    public void registerCommands() 
    {
        //For convenience' sake, we will initialize a variable.
        CommandHandler handler = new CommandHandler();
 
        //Registers the command /example which has no arguments.
        handler.register("cf2", new Cf2());
 
        //Registers the command /example args based on args[0] (args)
        handler.register("create", new Create());
        handler.register("setspawn", new Setspawn());
        handler.register("join", new Join());
        handler.register("leave", new Leave());
        handler.register("start", new Start());
        handler.register("stop", new Stop());
        handler.register("remove", new Remove());
        handler.register("restart", new Restart());
        handler.register("setup", new Setup());
        handler.register("help", new Help());
        handler.register("move", new Move());
        handler.register("kick", new Kick());
        handler.register("attach", new Attach());
        handler.register("skin", new Skin());
        getCommand("cf2").setExecutor(handler);

        this.getCommand("global").setExecutor(new Global());
		this.getCommand("ac").setExecutor(new Admin());
		this.getCommand("team").setExecutor(new Team());
    }
	
	@Override
    public void onEnable() 
    {
		plugin = this;
		protocol = ProtocolLibrary.getProtocolManager();
		registerCommands();
		registerEvents(this, new PlayerJoin());
		registerEvents(this, new PlayerChat());
		registerEvents(this, new PlayerLeave());
		ProtocolLibrary.getProtocolManager().addPacketListener(new PlayerInfoPacket());
		InitConfig();
		loadArenas();
		
	}
    
	@Override
    public void onDisable() 
	{
    	for (String str : ArenaManager.getManager().enabledArenas.keySet())
    	{
    		ArenaManager.getManager().stopArena(str);
    	}
		
		plugin = null;
	}
    
    public void InitConfig()
	{	
		config.addDefault("MySQL.enabled", false);
		config.addDefault("MySQL.hostname", "localhost");
		config.addDefault("MySQL.port", "3306");
		config.addDefault("MySQL.database", "com/drengi/craftfortress/CraftFortress");
		config.addDefault("MySQL.username", "User");
		config.addDefault("MySQL.password", "Password");
		
		config.addDefault("Settings.Debug_Enabled", true);
		
		config.options().copyDefaults(true);
		saveConfig();
	}
    
    public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) 
	{
	    for (Listener listener : listeners) 
	    {
	    	Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
	    }
	}
    
    public void loadArenas()
    {
		manager = Bukkit.getScoreboardManager();
		ArenaManager am = ArenaManager.getManager();
		am.loadMaps();
		am.loadArenas();
    }
    
    private Object getSessionService()
    {
        Server server = Bukkit.getServer();
        try
        {
            Object mcServer = server.getClass().getDeclaredMethod("getServer").invoke(server);
            for (Method m : mcServer.getClass().getMethods())
            {
                if (m.getReturnType().getSimpleName().equalsIgnoreCase("MinecraftSessionService"))
                {
                    return m.invoke(mcServer);
                }
            }
        }
        catch (Exception ex)
        {
            throw new IllegalStateException("An error occurred while trying to get the session service", ex);
        }
        throw new IllegalStateException("No session service found :o");
    }

    private Method getFillMethod(Object sessionService)
    {
        for(Method m : sessionService.getClass().getDeclaredMethods())
        {
            if(m.getName().equals("fillProfileProperties"))
            {
                return m;
            }
        }
        throw new IllegalStateException("No fillProfileProperties method found in the session service :o");
    }
}