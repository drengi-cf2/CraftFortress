package com.drengi.craftfortress.CraftFortress;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class Map 
{
	private boolean inUse;
	private boolean enabled;
	private String name;
	
	private String world;
	private Double maxX;
	private Double maxY;
	private Double maxZ;
	private Double minX;
	private Double minY;
	private Double minZ;
	
	private String gamemode;
	
	private File dataFile = null;
	private final FileConfiguration config = CraftFortressPlugin.plugin.getConfig();
	private YamlConfiguration mapConfig = null;
	private Logger log = CraftFortressPlugin.plugin.getLogger();
	private String configSection;
	
	public Map (String name)
	{
		this.name = name;
		this.configSection = "Maps." + name;
	}
	
	public void createMap(String world, double minX, double minY, double maxX, double maxY, String type)
	{	
		dataFile = new File(CraftFortressPlugin.plugin.getDataFolder() + File.separator + "maps" + File.separator + this.name + ".yml");
		try 
		{
			dataFile.createNewFile();
		} 
		catch (IOException e) 
		{
			log.warning("Unable to create an map file for " + this.name);
			e.printStackTrace();
		}
		
		mapConfig = YamlConfiguration.loadConfiguration(dataFile);
		
		mapConfig.set("Name", this.name);
		mapConfig.set("Location.world", world);
		mapConfig.set("Location.minX", minX);
		mapConfig.set("Location.minY", minY);
		mapConfig.set("Location.maxX", maxX);
		mapConfig.set("Location.maxY", maxY);
		mapConfig.set("Type", type);
		mapConfig.set("Entities.default", "nothing");
		mapConfig.set("Entities.default", null);
		
		config.set("Maps." + this.name, false);
		CraftFortressPlugin.plugin.saveConfig();
		try 
		{
			mapConfig.save(dataFile);
		} 
		catch (IOException e) 
		{
			log.warning("There was a problem saving the map file for " + this.name);
			e.printStackTrace();
		}
		
		log.info("Map \""+ this.name + "\" created.");
	}
	
	protected boolean loadMap()
	{
		dataFile = new File(CraftFortressPlugin.plugin.getDataFolder() + File.separator + "maps" + File.separator + this.name + ".yml");
		if (!dataFile.exists())
		{
			return false;
		}
		
		mapConfig = YamlConfiguration.loadConfiguration(dataFile);
		if (config.getBoolean(configSection) == false)
		{
			enabled = false;
		}
		else
		{
			enabled = true;
		}
		
		//Check if spawn is created, if it's not, hold it right there.
		if (this.checkIfMeetsRequirements())
		{
			enabled = true;
		}
		
		//Get Crucial Information from Config
		this.world = mapConfig.getString("Location.world");
		this.minX = mapConfig.getDouble("Location.minX");
		this.minY = mapConfig.getDouble("Location.minY");
		this.maxX = mapConfig.getDouble("Location.maxX");
		this.maxY = mapConfig.getDouble("Location.maxY");
		this.gamemode = mapConfig.getString("Type");
		return true;
	}
	
	public boolean checkIfMeetsRequirements()
	{
		if (mapConfig.getString("Spawns.RED.X") != null || mapConfig.getString("Spawns.BLU.X") != null)
		{
			return true;
		}
		return false;
	}
	
	public void createEntity(Player p, String ent, double x, double y, double z)
	{
		dataFile = new File(CraftFortressPlugin.plugin.getDataFolder() + File.separator + "maps" + File.separator + this.name + ".yml");
		mapConfig = YamlConfiguration.loadConfiguration(dataFile);
		
		mapConfig.set("Entities." + ent + ".x", x);
		mapConfig.set("Entities." + ent + ".y", y);
		mapConfig.set("Entities." + ent + ".z", z);
		try 
		{
			mapConfig.save(dataFile);
		} 
		catch (IOException e) 
		{
			log.warning("There was an issue saving the config file for " + name);
			e.printStackTrace();
		}
		CraftFortressPlugin.plugin.getLogger().info("Created entity for map " + name + ": " + ent);
	}
	
	public Location getSpawn(String team)
	{
		String spawn = "Spawns." + team;
		
		double x = mapConfig.getDouble(spawn + ".X");
		double y = mapConfig.getDouble(spawn + ".Y");
		double z = mapConfig.getDouble(spawn + ".Z");
		
		Location spawnLoc = new Location(Bukkit.getWorld(world), x, y, z);
				
		return spawnLoc;
	}
	
	public void setSpawn(String type, double X, double Y, double Z)
	{
		String spawn = "Spawns." + type;
		dataFile = new File(CraftFortressPlugin.plugin.getDataFolder() + File.separator + "maps" + File.separator + this.name + ".yml");
		mapConfig = YamlConfiguration.loadConfiguration(dataFile);
		
		mapConfig.set(spawn + ".X", X);
		mapConfig.set(spawn + ".Y", Y);
		mapConfig.set(spawn + ".Z", Z);
		try 
		{
			mapConfig.save(dataFile);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	public boolean isEnabled() 
	{
		return enabled;
	}
	
	public void enable()
	{
		enabled = true;
		config.set(configSection, true);
		CraftFortressPlugin.plugin.saveConfig();
	}
	
	public String getName()
	{
		return name;
	}
	
	
}
