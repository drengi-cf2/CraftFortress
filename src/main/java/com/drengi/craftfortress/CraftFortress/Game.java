package com.drengi.craftfortress.CraftFortress;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.drengi.craftfortress.gamemodes.CTF;
import com.drengi.craftfortress.gamemodes.GamemodeInterface;

public class Game
{
	//The only thing that this should be handling is location information,
	//player data, the world, the scoreboard, and type of arena it is.
	
	//Important Game Stuff
	private String name;
	private List<String> supportedMaps = new ArrayList<String>();
	private String type;
	private boolean enabled;
	private Map currentMap;
	
	//Scoreboards, teams, and objectives
	private Scoreboard board = CraftFortressPlugin.manager.getNewScoreboard();
	private ArenaManager manager = ArenaManager.getManager();
	private Objective objective = board.registerNewObjective("CraftFortress 2", "dummy");
	private Team BLU = board.registerNewTeam("RED");
	private Team RED = board.registerNewTeam("BLU");
	private Team SPEC = board.registerNewTeam("SPECTATOR");
    private Score score = objective.getScore(ChatColor.GREEN + "Map: " + name);

	//Player Management
	private HashMap<UUID, String> players = new HashMap<UUID, String>();
	private int maxPlayersPerTeam = 12;
	private int maxPlayers = 24;
	
	//Configuration variables
	private final FileConfiguration config = CraftFortressPlugin.plugin.getConfig();
	private FileConfiguration arenaConfig = null;
	private File dataFile = null;
	
	//Loggers
	private Logger log = CraftFortressPlugin.plugin.getLogger();
	private String configSection;
	
	//Possible com.drengi.craftfortress.gamemodes
    private GamemodeInterface gm = null;
	
	public Game(String name)
	{
		this.name = name;
		this.configSection = "Arenas." + name;
	}

	public void createArena(String type)
	{	
		dataFile = new File(CraftFortressPlugin.plugin.getDataFolder() + File.separator + "arenas" + File.separator + this.name + ".yml");
		try 
		{
			dataFile.createNewFile();
		} 
		catch (IOException e) 
		{
			log.warning("Unable to create an arena file for " + this.name);
			e.printStackTrace();
		}
		
		arenaConfig = YamlConfiguration.loadConfiguration(dataFile);
		
		arenaConfig.set("Name", this.name);
		arenaConfig.set("Type", type);
		arenaConfig.set("SupportedMaps", supportedMaps);
		arenaConfig.set("Settings.MaxPlayersPerTeam", 12);
		arenaConfig.set("Settings.MaxPlayers", 24);
		
		config.set("Arenas." + this.name, false);
		enabled = false;
		CraftFortressPlugin.plugin.saveConfig();
		try 
		{
			arenaConfig.save(dataFile);
		} 
		catch (IOException e) 
		{
			log.warning("There was a problem saving the arena file for " + this.name);
			e.printStackTrace();
		}
		
		log.info("Game \""+ this.name + "\" created.");
	}
	
	protected boolean loadArena()
	{
		dataFile = new File(CraftFortressPlugin.plugin.getDataFolder() + File.separator + "arenas" + File.separator + this.name + ".yml");
		if (!dataFile.exists())
		{
			return false;
		}
		
		arenaConfig = YamlConfiguration.loadConfiguration(dataFile);
		if (config.getBoolean(configSection) == false)
		{
			enabled = false;
		}
		else
		{
			enabled = true;
		}
		
		//Check if spawn is created, if it's not, hold it right there.
		if (arenaConfig.getString("Spawns.RED.X") == null || arenaConfig.getString("Spawns.BLU.X") == null)
		{
			return false;
		}
		
		//Get Crucial Information from Config
		this.type = arenaConfig.getString("Type");
		this.maxPlayers = arenaConfig.getInt("Settings.MaxPlayers");
		this.maxPlayersPerTeam = arenaConfig.getInt("Settings.MaxPlayersPerTeam");
		this.supportedMaps = arenaConfig.getStringList("SupportedMaps");
		
		//Gamemode
        
		if (type.equalsIgnoreCase("ctf"))
		{
			gm = new CTF(this);
		}
		
		//Setup BLU
		BLU.setPrefix(ChatColor.DARK_BLUE + "[BLU] " + ChatColor.RESET);
		BLU.setDisplayName(ChatColor.DARK_BLUE + "BLU" + ChatColor.RESET);
		BLU.setCanSeeFriendlyInvisibles(true);
		BLU.setAllowFriendlyFire(false);
		BLU.setNameTagVisibility(NameTagVisibility.HIDE_FOR_OTHER_TEAMS);
		
		//Setup RED
		RED.setPrefix(ChatColor.DARK_RED + "[RED] " + ChatColor.RESET);
		RED.setDisplayName(ChatColor.DARK_RED + "RED" + ChatColor.RESET);
		RED.setCanSeeFriendlyInvisibles(true);
		RED.setAllowFriendlyFire(false);
		BLU.setNameTagVisibility(NameTagVisibility.HIDE_FOR_OTHER_TEAMS);
		
		//Setup Spectator
		SPEC.setPrefix(ChatColor.WHITE + "[SPEC] " + ChatColor.RESET);
		SPEC.setDisplayName(ChatColor.WHITE + "SPEC" + ChatColor.RESET);
		SPEC.setCanSeeFriendlyInvisibles(true);
		SPEC.setAllowFriendlyFire(false);
		BLU.setNameTagVisibility(NameTagVisibility.HIDE_FOR_OTHER_TEAMS);
		
		//Setup Objective
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName("com/drengi/craftfortress/CraftFortress");
		
		//Set score
		score.setScore(1);
		return true;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public Location getSpawn(String team)
	{
		return currentMap.getSpawn(team);
	}
	
	public String getType()
	{
		return this.type;
	}
	
	public HashMap<UUID, String> getPlayerData()
	{
		return this.players;
	}
	
	public Team getBLU()
	{
		return this.BLU;
	}
	
	public Team getRED()
	{
		return this.RED;
	}
	
	public Team getSpec()
	{
		return this.SPEC;
	}
	
	public Score getScore()
	{
		return this.score;
	}
	
	public Scoreboard getBoard()
	{
		return this.board;
	}
	
	public FileConfiguration getConfig()
	{
		return this.arenaConfig;
	}
	
	public GamemodeInterface getGamemode()
	{
		return gm;
	}
	
	public Map getMap()
	{
		return currentMap;
	}
	
	public boolean isEnabled()
	{
		return enabled;
	}
	
	public void enableArena()
	{
		enabled = true;
		config.set("Arenas." + this.name, true);
		CraftFortressPlugin.plugin.saveConfig();
	}
	
	public void attachMap(Map map)
	{
		supportedMaps.add(map.getName());
	}
	
	public boolean switchMap(Map map)
	{
		//TODO: Implement this
		return true;
	}
	
	public boolean hasMaps()
	{
		if (supportedMaps.size() > 0)
		{
			return true;
		}
		return false;
	}
	
	public void start()
	{
		if (supportedMaps.size() > 0 && gm.isReadyToStart() && enabled)
		{
			for (String map : supportedMaps)
			{
				if (manager.checkIfMapAvailable(map))
				{
					this.currentMap = manager.getMap(map);
					gm.setupMap();
					gm.startRound();
				}
			}
		}
	}
}
