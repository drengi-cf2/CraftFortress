package com.drengi.craftfortress.chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.ArenaManager;

public class Team implements CommandExecutor
{
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{	
		if (cmd.getName().equalsIgnoreCase("team"))
		{
			if (!(sender instanceof Player))
            {
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < args.length; i++)
				{
					sb.append(args[i]).append(" ");
				}
				String m = sb.toString().trim();
				
				if (!(sender instanceof Player))
	            {
	                String p = "AutoMod";
					Bukkit.broadcastMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + "GLOBAL" + ChatColor.DARK_GRAY + "] " + ChatColor.WHITE + p + ": " + ChatColor.RESET + m);
	            }
            }
			else
			{
				Player p = (Player) sender;
				
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < args.length; i++)
				{
					sb.append(args[i]).append(" ");
				}
				 
				String m = sb.toString().trim();
				ArenaManager.getManager().chatTeam(p, m);
			}
		}
		return true;
	}
}
