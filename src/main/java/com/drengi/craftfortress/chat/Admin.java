package com.drengi.craftfortress.chat;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.drengi.craftfortress.CraftFortress.ArenaManager;

public class Admin implements CommandExecutor 
{
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{	
		if (cmd.getName().equalsIgnoreCase("ac"))
		{
			if (!(sender instanceof Player))
            {
				ArenaManager am = ArenaManager.getManager();
				StringBuilder sb = new StringBuilder();
				
				for (int i = 0; i < args.length; i++)
				{
					sb.append(args[i]).append(" ");
				}
				 
				String m = sb.toString().trim();
				am.chatAdmin("AutoMod", m);
            }
			else
			{
				Player player = (Player) sender;
				
				if (player.hasPermission("craftfortress.com.drengi.craftfortress.chat.admin"))
				{
					ArenaManager am = ArenaManager.getManager();
					StringBuilder sb = new StringBuilder();
					
					for (int i = 0; i < args.length; i++)
					{
						sb.append(args[i]).append(" ");
					}
					 
					String m = sb.toString().trim();
					am.chatAdmin(player.getDisplayName(), m);
				}
				else
				{
					player.sendMessage("You don't have permission for this.");
				}
			}
		}
		return true;
	}
}
