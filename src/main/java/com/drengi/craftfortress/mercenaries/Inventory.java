package com.drengi.craftfortress.mercenaries;

import java.util.HashMap;

import org.bukkit.entity.Player;

public class Inventory
{
	private HashMap<String, Item> inventory = new HashMap<String, Item>();
	private Player player;
	
	protected Inventory(Player p)
	{
		this.player = p;
		this.fetchInventory();
	}
	
	private void fetchInventory()
	{
		//TODO: Connect to mySQL database for inventories
	}
	
	protected void addItem(String cName, String quality, boolean isKillstreak)
	{
		//TODO: Connect to mySQL database to check if item template exists, then have the magical item checker do it.
	}
	
	protected HashMap<String, Item> getInventory()
	{
		return this.inventory;
	}
}