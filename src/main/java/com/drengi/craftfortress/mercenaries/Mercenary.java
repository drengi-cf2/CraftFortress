package com.drengi.craftfortress.mercenaries;

import org.bukkit.entity.Player;

public class Mercenary
{
	private Player p = null;
	private Inventory inv = null;
	
	public Mercenary(Player player)
	{
		p = player;
		this.loadInventory();
		//TODO: Connect to mySQL database to see if the player has been here before.
		//If they have, load info.
		//If not, create a new one for them
	}

	private void loadInventory()
	{
		inv = new Inventory(p);
	}
	
	public Inventory getInventory()
	{
		return inv;
	}
	
	
	
	
}