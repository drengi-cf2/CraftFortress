package com.drengi.craftfortress.mercenaries;

public class Item
{
	private String name;
	private String level;
	private String cleanName;
	private String[] classes;
	private String quality;
	
	private boolean isStrange;
	private boolean isKillstreak;
	
	private int kills;
	private int id;
	
	public Item(String cName)
	{
		//Weapon is new
		//TODO: Connect to mySQL database about this weapon, populate. If it doesn't find it, log error
	}
	
	public Item(int id)
	{
		//Weapon is not new, it was found in an inventory.
	}
	
	protected String getName()
	{
		return this.name;
	}
	
	protected String getLevel()
	{
		return this.level;
	}
	
	protected String getCleanName()
	{
		return this.cleanName;
	}
	
	protected boolean isUsableByClass(String c) 
	{
		for (String Class : this.classes)
		{
			if (Class.equalsIgnoreCase(c)) return true;
		}
		return false;
	}
	
	protected String getQuality()
	{
		return this.quality;
	}
	
	protected boolean getIsStrange()
	{
		return this.isStrange;
	}
	
	protected boolean getIsKillstreak()
	{
		return this.isKillstreak;
	}
	
	protected int getKills()
	{
		if (this.isStrange) return this.kills;
		return 0;
	}
	
	protected void setKills(int kills)
	{
		if (this.isStrange) this.kills = kills;
	}
}